#!/usr/bin/env python3
""" Image and data processing functions """
__author__ = 'Henry A Leopold'

# ==============================================================================
# Imports
# ------------------------------------------------------------------------------
import numpy as np
import random
from scipy.misc import imresize, toimage

# ==============================================================================
# Contents
# ------------------------------------------------------------------------------
__all__ = [
        # ===================
        # Image Preprocessing
        # -------------------
            'rgb2grey',
            'shift_distribution',
            'preprocess_state',
]

# ==============================================================================
# Variables
# ------------------------------------------------------------------------------

# ==============================================================================
# Image Preprocessing Functions
# ------------------------------------------------------------------------------    
def rgb2grey(image):
    """ Simple RGB to grey-space conversion
    """
    return np.dot(image[...,:3], [0.299, 0.587, 0.114])

def shift_distribution(image, scale=(-.5,.5)):
    """ Rescales the pixel intensities / array values to a new distribution. 
    Args
        image (numpy array): Single channel image
        scale (tuple): Min-Max range for shifting the intensities.
    Returns
        rescaled image
    """
    old_min = image.min()
    old_max = image.max()
    shift = lambda p: (((p - old_min) * (scale[1] - scale[0])) / (old_max - old_min)) + scale[0]
    return shift(image)
    
def preprocess_state(state, grey=False, shift=None, rescale=None, NCHW=True):
    """ Wrapper for preprocessing the state.
    Args
        state (image/array): Current observable environmental state.
        rgb2grey (boolean): Flag for converting an image from rbg to grey scale
        shift (tuple): Min-Max range for shifting the data distribution 
                        (of an array or single channel image)
        rescale (tuple of ints): (x, y) dimensions for image
        NCHW (boolean): Flag for channel dimension; either NCHW or NHWC
    Returns
        rescaled image
    """
    new_state = np.squeeze(state)
    dims = state.ndim
    
    # Begin preprocessing
    if rescale:
        # Resize the image to the new size and extract the 'Y' channel
        new_state = imresize(new_state, rescale)
    if grey:
        # Convert to grey scale image
        new_state = rgb2grey(new_state)
        if shift:
            # Shift the array/pixel values to the new range
            new_state = shift_distribution(new_state, shift)
    else: 
        # Induce shift in intensities if provided
        h, l = shift if shift else (new_state.max(), new_state.min())
        # Extract the Y channel as with DQN
        new_state = np.array(toimage(new_state, high=h, low=l, mode='YCbCr'))
        new_state = new_state[...,0]
    if new_state.ndim < dims:
        # If you've lost a dimension during channel manipulations
        new_state = np.expand_dims(new_state, 0) if NCHW else np.expand_dims(new_state, -1)
    
    # Add the batch dimention (N) and return the preprocessed state
    return np.expand_dims(new_state, 0)
