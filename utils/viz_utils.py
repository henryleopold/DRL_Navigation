#!/usr/bin/env python3
""" Visualization Scripts """
__author__ = 'Henry Leopold'

# ==============================================================================
# Imports
# ------------------------------------------------------------------------------
import sys
import numpy as np
import pandas as pd
import matplotlib.collections as mc
from IPython import display
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.patches import Rectangle

# ==============================================================================
# Contents
# ------------------------------------------------------------------------------

# __all__ = [
#         # ===================
#         # Agent related
#         # -------------------
#             'show_state',
#             'visualize_samples',
#             'plot_scores',
#             'plot_rolling_scores',
#             'plot_q_table',
#             'visualize_tilings',
#         # ===================
#         # General/Sample
#         # -------------------
#             'visualize_encoded_samples',
#             'plot_kpi',
#             'viz_samples',
#             'show_images',
#             'layer_viz',
#             'show_activation',
#             'show_detailed_image'
# ]

# ==============================================================================
# Misc Related
# ------------------------------------------------------------------------------
def in_ipython():
    """ Check for if code is executed in iPython notebook """
    try:
        __IPYTHON__
        return True
    except NameError:
        return False

def check_platform():
    """ Check the current platform """
    platform = sys.platform
    if platform == "linux" or platform == "linux2":
        # linux
        print('linux')
    elif platform == "darwin":
        # OS X
        print('osx')
    elif platform == "win32":
       # Windows
        print('windows')

def on_mac():
    return True if check_platform() == 'osx' else False

# ==============================================================================
# Agent Related
# ------------------------------------------------------------------------------
def show_state(env, step=0, info=""):
    """ Shows the environment in jupyter notebook.
    Need to have launched jupyter with: xvfb-run -s "-screen 0 1400x900x24" jupyter notebook
    Modified from: Andrew Schreiber - https://stackoverflow.com/questions/40195740/how-to-run-openai-gym-render-over-a-server

    Alternative methods:
        http://answerqueen.com/d1/m1yj68o3d1
        https://gist.github.com/8enmann/931ec2a9dc45fde871d2139a7d1f2d78

    Args
        env (gym): Gym environment. Depending on the version, it needs to be unwrapped.
        step (int): Step/episode count to display.
        info (string): Additional info to display.
    """
    # from IPython import display

    plt.figure(3)
    plt.clf()
    plt.imshow(env.render(mode='rgb_array'))
    plt.title("%s | Step: %d %s" % (env.spec.id,step, info))
    plt.axis('off')

    display.display(plt.gcf())
    display.clear_output(wait=True)


def visualize_samples(samples, discretized_samples, grid, low=None, high=None):
    """Visualize original and discretized samples on a given 2-dimensional grid.
    Args
        samples (array list): A list of sample arrays for the (original) continuous space.
        discretized samples (array_like): A sequence of integers with the same number of dimensions as sample.
        grid (array list): A list of arrays containing split points for each dimension.
        low (float list): Lower bounds for each dimension of the continuous space.
        high (float list): Upper bounds for each dimension of the continuous space.
    """
    # import matplotlib.collections as mc

    fig, ax = plt.subplots(figsize=(10, 10))

    # Show grid
    ax.xaxis.set_major_locator(plt.FixedLocator(grid[0]))
    ax.yaxis.set_major_locator(plt.FixedLocator(grid[1]))
    ax.grid(True)

    # If bounds (low, high) are specified, use them to set axis limits
    if low is not None and high is not None:
        ax.set_xlim(low[0], high[0])
        ax.set_ylim(low[1], high[1])
    else:
        # Otherwise use first, last grid locations as low, high (for further mapping discretized samples)
        low = [splits[0] for splits in grid]
        high = [splits[-1] for splits in grid]

    # Map each discretized sample (which is really an index) to the center of corresponding grid cell
    grid_extended = np.hstack((np.array([low]).T, grid, np.array([high]).T))  # add low and high ends
    grid_centers = (grid_extended[:, 1:] + grid_extended[:, :-1]) / 2  # compute center of each grid cell
    locs = np.stack(grid_centers[i, discretized_samples[:, i]] for i in range(len(grid))).T  # map discretized samples

    ax.plot(samples[:, 0], samples[:, 1], 'o')  # plot original samples
    ax.plot(locs[:, 0], locs[:, 1], 's')  # plot discretized samples in mapped locations
    ax.add_collection(mc.LineCollection(list(zip(samples, locs)), colors='orange'))  # add a line connecting each original-discretized sample
    ax.legend(['original', 'discretized'])

def plot_scores(scores):
    """Plots the scores from training using matplotlib.
    Args
        scores (list): Numerical list of scores (rewards) during training.
    """
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.plot(np.arange(len(scores)), scores)
    plt.ylabel('Score')
    plt.xlabel('Episode #')
    plt.show()
    
def plot_rolling_scores(scores, rolling_window=100):
    """Plot scores and optional rolling mean using specified window.
    Args
        scores (list): List of episode scores (usually floats)
        rolling_window (int): Window size
    """
    plt.plot(scores); plt.title("Scores");
    rolling_mean = pd.Series(scores).rolling(rolling_window).mean()
    plt.plot(rolling_mean);
    return rolling_mean

#TODO make generic?
def plot_q_table(q_table, fsize=10, title='Q-table', xlabel='position', ylabel='velocity', cmap='jet'):
    """Visualize max Q-value for each state and corresponding action.
    Args
        fsize (int): Size of figure (assumes square).
        title (str): Title for plot
        xlabel (str): X-axis label
        ylabel (str): Y-axis label
        cmap (str): Colour map to use
    """
    q_image = np.max(q_table, axis=2)       # max Q-value for each state
    q_actions = np.argmax(q_table, axis=2)  # best action for each state

    fig, ax = plt.subplots(figsize=(fsize, fsize))
    cax = ax.imshow(q_image, cmap=cmap);
    cbar = fig.colorbar(cax)
    for x in range(q_image.shape[0]):
        for y in range(q_image.shape[1]):
            ax.text(x, y, q_actions[x, y], color='white',
                    horizontalalignment='center', verticalalignment='center')
    ax.grid(False)
    ax.set_title("{}, size: {}".format(title, q_table.shape))
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

def visualize_tilings(tilings, fsize=10, title='Tilings'):
    """Plot each tiling as a grid.
    Args
        tilings (list): A list of tilings (grids), each produced by create_tiling_grid().
        fsize (int): Size of figure (assumes square).
        title (str): Title for plot
    """
    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']
    linestyles = ['-', '--', ':']
    legend_lines = []

    fig, ax = plt.subplots(figsize=(10, 10))
    for i, grid in enumerate(tilings):
        for x in grid[0]:
            l = ax.axvline(x=x, color=colors[i % len(colors)], linestyle=linestyles[i % len(linestyles)], label=i)
        for y in grid[1]:
            l = ax.axhline(y=y, color=colors[i % len(colors)], linestyle=linestyles[i % len(linestyles)])
        legend_lines.append(l)
    ax.grid('off')
    ax.legend(legend_lines, ["Tiling #{}".format(t) for t in range(len(legend_lines))], facecolor='white', framealpha=0.9)
    ax.set_title(title)
    return ax  # return Axis object to draw on later, if needed

# ==============================================================================
# General Sample
# ------------------------------------------------------------------------------
def visualize_encoded_samples(samples, encoded_samples, tilings, low=None, high=None, title="Tile-encoded samples"):
    """Visualize samples by activating the respective tiles."""
    # from matplotlib.patches import Rectangle

    samples = np.array(samples)  # for ease of indexing

    # Show tiling grids
    ax = visualize_tilings(tilings)

    # If bounds (low, high) are specified, use them to set axis limits
    if low is not None and high is not None:
        ax.set_xlim(low[0], high[0])
        ax.set_ylim(low[1], high[1])
    else:
        # Pre-render (invisible) samples to automatically set reasonable axis limits, and use them as (low, high)
        ax.plot(samples[:, 0], samples[:, 1], 'o', alpha=0.0)
        low = [ax.get_xlim()[0], ax.get_ylim()[0]]
        high = [ax.get_xlim()[1], ax.get_ylim()[1]]

    # Map each encoded sample (which is really a list of indices) to the corresponding tiles it belongs to
    tilings_extended = [np.hstack((np.array([low]).T, grid, np.array([high]).T)) for grid in tilings]  # add low and high ends
    tile_centers = [(grid_extended[:, 1:] + grid_extended[:, :-1]) / 2 for grid_extended in tilings_extended]  # compute center of each tile
    tile_toplefts = [grid_extended[:, :-1] for grid_extended in tilings_extended]  # compute topleft of each tile
    tile_bottomrights = [grid_extended[:, 1:] for grid_extended in tilings_extended]  # compute bottomright of each tile

    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']
    for sample, encoded_sample in zip(samples, encoded_samples):
        for i, tile in enumerate(encoded_sample):
            # Shade the entire tile with a rectangle
            topleft = tile_toplefts[i][0][tile[0]], tile_toplefts[i][1][tile[1]]
            bottomright = tile_bottomrights[i][0][tile[0]], tile_bottomrights[i][1][tile[1]]
            ax.add_patch(Rectangle(topleft, bottomright[0] - topleft[0], bottomright[1] - topleft[1],
                                   color=colors[i], alpha=0.33))

            # In case sample is outside tile bounds, it may not have been highlighted properly
            if any(sample < topleft) or any(sample > bottomright):
                # So plot a point in the center of the tile and draw a connecting line
                cx, cy = tile_centers[i][0][tile[0]], tile_centers[i][1][tile[1]]
                ax.add_line(Line2D([sample[0], cx], [sample[1], cy], color=colors[i]))
                ax.plot(cx, cy, 's', color=colors[i])

    # Finally, plot original samples
    ax.plot(samples[:, 0], samples[:, 1], 'o', color='r')

    ax.margins(x=0, y=0)  # remove unnecessary margins
    ax.set_title(title)
    return ax

def show_images(images, lables, preds=None, num_rows=2, cmap='gray'):
    """ Plots images in a batch, along with predicted and true labels.
    Args
        images (numpy array): Stack of images
        labels (numpy array/vector): Ground truth labels for the images
        preds (numpy array/vector): Predictions corresponding to the images. If they aren't provided, images are plotted with only labels.
        num_rows (int): number of rows to display
        cmap (str): Colour map to use

    """
    # TODO - make smarter // add asserts
    batch_size = len(preds)
    fig = plt.figure(figsize=(25, 4))
    for idx in np.arange(batch_size):
        ax = fig.add_subplot(num_rows, batch_size/num_rows, idx+1, xticks=[], yticks=[])
        ax.imshow(np.squeeze(images[idx]), cmap=cmap)
        if preds is not None:
            ax.set_title("{} ({})".format(classes[preds[idx]], classes[labels[idx]]),
                     color=("green" if preds[idx]==labels[idx] else "red"))
        else:
            ax.set_title(classes[labels[idx]])

def layer_viz(layer, num_features=10, num_rows=2,  cmap='gray', title=None):
    """ Plots some features in a layer.
    Args
        layer (torch layer):
        num_features (int): number of features to sample
        num_rows (int): number of rows to display
        cmap (str): Colour map to use
        title (str): Title to diplay
    """
    weights = layer.weight.data
    w = weights.numpy()

    fig=plt.figure(figsize=(20, 8))
    columns = num_features//num_rows
    rows = num_rows
    for i in range(0, num_features):
        fig.add_subplot(rows, columns, i+1)
        plt.imshow(w[i][0], cmap=cmap)

    print('First convolutional layer')
    plt.show()

def show_activation(layer, image, num_features=10, num_rows=2,  cmap='gray', title=None):
    """ Use OpenCV's filter2D function to apply a specific set of filter weights to a test image.
     Args
        layer (torch layer):
        image (image): from a dataset
        num_features (int): number of features to sample
        num_rows (int): number of rows to display
        cmap (str): Colour map to use
        title (str): Title to diplay

    """
    weights = layer.weight.data
    w = weights.numpy()

    fig=plt.figure(figsize=(20, 8))
    columns = num_features # TODO make smarter
    rows = num_rows
    for i in range(0, num_features):
        fig.add_subplot(rows, columns, i+1)
        if ((i%2)==0):
            plt.imshow(w[int(i/2)][0], cmap=cmap)
        else:
            c = cv2.filter2D(img, -1, w[int((i-1)/2)][0])
            plt.imshow(c, cmap=cmap)
    plt.show()

def show_detailed_image(img, fsize=10, title=None):
    """ Use OpenCV's filter2D function to apply a specific set of filter weights to a test image.
    Args
        image (image): from a dataset
        fsize (int):
        title (str): Title to diplay

    """
    fig = plt.figure(figsize = (fsize,fsize))
    ax = fig.add_subplot(111)
    ax.imshow(img, cmap='gray')
    width, height = img.shape
    thresh = img.max()/2.5
    for x in range(width):
        for y in range(height):
            val = round(img[x][y],2) if img[x][y] !=0 else 0
            ax.annotate(str(val), xy=(y,x),
                        horizontalalignment='center',
                        verticalalignment='center',
                        color='white' if img[x][y]<thresh else 'black')

def plot_kpi(kpi, xlabel='batches', ylabel='kpi'):
    """ Visualize the kpi in a line diagram.
     Args
        kpi (array): indicators to plot
        xlabel, ylabel (str): Axis labels
    """
    plt.plot(kpi)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.ylim(0, max(kpi)*1.25)
    plt.show()

def viz_samples(model, loader):
    """ Visualize a small batch of images and predicted classes.
     Args
        model (Net):
        loader (DataLoader):
    """
    # obtain one batch of test images
    dataiter = iter(loader)
    images, labels = dataiter.next()
    # get predictions
    preds = np.squeeze(model(images).data.max(1, keepdim=True)[1].numpy())
    images = images.numpy()

    # plot the images in the batch, along with predicted and true labels
    fig = plt.figure(figsize=(25, 4))
    for idx in np.arange(loader.batch_size):
        ax = fig.add_subplot(2, loader.batch_size/2, idx+1, xticks=[], yticks=[])
        ax.imshow(np.squeeze(images[idx]), cmap='gray')
        ax.set_title("{} ({})".format(model.classes[preds[idx]], model.classes[labels[idx]]),
                     color=("green" if preds[idx]==labels[idx] else "red"))
