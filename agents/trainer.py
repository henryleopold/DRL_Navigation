#!/usr/bin/env python3
""" Training and evaluation scripts """
__author__ = 'Henry A. Leopold'

# ==========================================================================================
# Imports
# ------------------------------------------------------------------------------------------
import sys, os, inspect, random
import gym
import torch
import numpy as np

from collections import deque
import matplotlib.pyplot as plt
from utils.viz_utils import on_mac
from utils.processing import preprocess_state

from unityagents import UnityEnvironment
# ==========================================================================================
# Contents
# ------------------------------------------------------------------------------------------
__all__ = [
        # ===================
        #  OpenAI Gym
        # -------------------
            'watch_agent',
            'train_gym',
        # ===================
        #  Unity ML-Agents
        # -------------------
            'build_unity',
            'train_unity',
]

# ==========================================================================================
# 
# ------------------------------------------------------------------------------------------



# ==========================================================================================
    # OpenAI / Gym
# ------------------------------------------------------------------------------------------
def watch_agent(agent, env, max_t=200):
    """ Watch an agent in the gym environment.
    Args:
        agent (Agent): Local agent class.
        env (Gym Environment): Environment needs to be initialized and running.
        max_t (int): Maximum number of time-steps the agent should act thru.
    """
    state = env.reset()
    for j in range(max_t):
        action = agent.act(state)

        if on_mac: 
            env.render()
        else:
            show_state(env, j)

        state, reward, done, _ = env.step(action)
        if done:
            break 
    env.close()

    
def train_gym(agent, env, visual = False, training = True, n_episodes=2000, max_t=1000, target_reward=200.0, logdir='logdir', shift=(-.5,.5), show=False, save=False, close=True):
    """ Training wrapper for OpenAI Gym environments. 
    Args:
        agent (Agent): Local agent class.
        env (Gym Environment): Environment needs to be initialized.
        visual (boolean): Flag for visual or vector Unity environments.
        training (boolean): Unity/ML-agent behavioural flag.
        n_episodes (int): maximum number of training episodes.
        max_t (int): maximum number of timesteps per episode.
        target_reward (int or float): Average reward in an episode considered to be a success.
        logdir (str): Directory for storing logs and checkpoints.
        shift (tuple): Min-Max range for shifting the intensities.
        show (boolean): Flag to show a snapshot of the env. 
                        - CAUTION: Can drastically reduce efficiency.
        save (boolean): Flag to save the agent on completion.
        close (boolean): Flag to close the environment on completion.
    
    Returns
        reward history (list): The score history the agent experienced throughout training.
    
    Returns:
        
    """
    try: 
        start = max(1,len(agent.episode_reward_history))
    except:
        start = 1
        
    for i_episode in range(start, n_episodes+1):
        state = env.reset()
        state = preprocess_state(np.squeeze(state), rescale=(84,84), shift=shift) if visual else state 
        for t in range(max_t):
            action = agent.act(state)
            next_state, reward, done, _ = env.step(action)
            next_state = preprocess_state(np.squeeze(next_state), rescale=(84,84), shift=shift) if visual else next_state 
            agent.step(state, action, reward, next_state, done)
            state = next_state
            if done:
                break 
                
        # Reset current reward and decrease epsilon after each episode
        agent.end_episode()           

        # Log episode results to notebook
        print('\rEpisode {}\tAverage Score: {:.2f}\tAgent Epsilon: {:.2f}'.format(i_episode,agent.avg_reward, agent.epsilon), end="")
        if i_episode % agent.reward_window == 0:
            print('\rEpisode {}\tAverage Score: {:.2f}\tAgent Epsilon: {:.2f}'.format(i_episode,agent.avg_reward, agent.epsilon))
        if show:
            try:
                plt.imshow(np.squeeze(state))
                plt.show()
            except:
                print(state)
                
        # Early stop condition based on target average score over a predefined episode window
        if agent.avg_reward>=target_reward:
            print('\nEnvironment solved in {:d} episodes!\tAverage Score: {:.2f}'.format(i_episode-agent.reward_window, agent.avg_reward))
            if save: agent.save(agent.filepath+f'_@target({target_reward})in{i_episode}eps')
            break
    if close: env.close()
    return agent.episode_reward_history

# ==========================================================================================
# Unity3D / ML-Agents
# ------------------------------------------------------------------------------------------
def build_unity(file_name, seed=0, no_graphics=False, train_mode=True, logdir='logdir', agent_config=None):
    """ Wrapper for creating a unity environment.
    Args:
        file_name (str): Path to the Unity enviroment, including file name/ext.
        agentClass (Agent): Local agent class
        seed (int): Random seed
        no_graphics (boolean): Flag for Unity graphics
        train_mode (boolean): Flag for Unity gradient freeze
        logdir (str): Directory path for saving logs and checkpoints. Default is 'logs'
        kwargs: Additional agent k:v hyperparameter pairs.
    Returns
        (agent) or (state_size and action_size) if an agentClass is not provided
        brain_name (str): Unity brain name
        env (UnityEnvironment)
    """
    # Build the environment
    env = UnityEnvironment(file_name=file_name, no_graphics=no_graphics, seed=seed)

    # Get the default brain
    brain_name = env.brain_names[0]
    brain = env.brains[brain_name]
    
    # Reset the environment
    env_info = env.reset(train_mode=train_mode)[brain_name]

    # Number of agents in the environment
    print('Number of agents: {}'.format(len(env_info.agents)))
    
    # Number of actions
    action_size = brain.vector_action_space_size    
    print(f'Number of actions: {action_size}')
    
    # Examine the state space 
    try:
        state = env_info.visual_observations[0]
        if not no_graphics:
            print('States look like:')
            plt.imshow(np.squeeze(state))
            plt.show()
    
    except:
        state = env_info.vector_observations[0]
        print('States look like:', state)
    
    state_size = len(state)
    print(f'States have length: {state_size}')
    
    if agent_config:
        try:
            agentClass = agent_config.pop('agentClass')
            try:
                agent_path = agent_config['filepath']
            except KeyError:
                agent_path = inspect.signature(agentClass).parameters['filepath'].default
            agent_config['filepath'] = os.path.join(logdir, agent_path)
            agent_config['action_size']=action_size
            agent_config['state_size']=state_size
            agent = agentClass(**agent_config)
            return agent, brain_name, env
        except:
            env.close()
            raise ValueError('Agent parameters are not properly specified.')
    return state_size, action_size, brain_name, env



def train_unity(agent, brain_name, env, visual = False, training = True, n_episodes=2000, max_t=1000, target_reward=200.0, logdir='logdir', shift=(-.5,.5), show=False, save=False, close=True):
    """Unity - ML-Agents training procedure using pytorch.
    Args:
        agent (Agent): Local agent classes, built with Pytorch.
        brain_name (str): Unity/ML-agent brain name
        env (Unity/ML-agents Environment): Compiled ahead of project by DRLND. 
                        - May need to be refactored later.
        visual (boolean): Flag for visual or vector Unity environments.
        training (boolean): Unity/ML-agent behavioural flag.
        n_episodes (int): maximum number of training episodes.
        max_t (int): maximum number of timesteps per episode.
        target_reward (int or float): Average reward in an episode considered to be a success.
        logdir (str): Directory for storing logs and checkpoints.
        shift (tuple): Min-Max range for shifting the intensities.
        show (boolean): Flag to show a snapshot of the env. 
                        - CAUTION: Can drastically reduce efficiency.
        save (boolean): Flag to save the agent on completion.
        close (boolean): Flag to close the environment on completion.
    
    Returns
        reward history (list): The score history the agent experienced throughout training.
    """
    try: 
        start = max(1,len(agent.episode_reward_history))
    except:
        start = 1
    
    for i_episode in range(start, n_episodes+1):
        env_info = env.reset(train_mode=training)[brain_name]
        state = preprocess_state(np.squeeze(env_info.visual_observations[0]), rescale=(84,84), shift=shift) if visual else env_info.vector_observations[0]
        for t in range(max_t):
            action = agent.act(state)
            env_info = env.step(action)[brain_name]
            next_state = preprocess_state(np.squeeze(env_info.visual_observations[0]), rescale=(84,84), shift=shift) if visual else env_info.vector_observations[0]  
            reward = env_info.rewards[0]
            done = env_info.local_done[0]
            agent.step(state, action, reward, next_state, done)
            state = next_state
            if done:
                break
        # Reset current reward and decrease epsilon after each episode
        agent.end_episode()           

        # Log episode results to notebook
        print('\rEpisode {}\tAverage Score: {:.2f}\tAgent Epsilon: {:.2f}'.format(i_episode,agent.avg_reward, agent.epsilon), end="")
        if i_episode % agent.reward_window == 0:
            print('\rEpisode {}\tAverage Score: {:.2f}\tAgent Epsilon: {:.2f}'.format(i_episode,agent.avg_reward, agent.epsilon))
        if show:
            try:
                plt.imshow(np.squeeze(state))
                plt.show()
            except:
                print(state)
                
        # Early stop condition based on target average score over a predefined episode window
        if agent.avg_reward>=target_reward:
            print('\nEnvironment solved in {:d} episodes!\tAverage Score: {:.2f}'.format(i_episode-agent.reward_window, agent.avg_reward))
            break
    if close: env.close()
    if save: 
        agent.save(agent.filepath+f'_@target({target_reward})in{i_episode}eps')
    return agent.episode_reward_history