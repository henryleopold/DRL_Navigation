#!/usr/bin/env python3
""" Memory classes and related functions """
__author__ = 'Henry Leopold'

# ==============================================================================
# Imports
# ------------------------------------------------------------------------------
import numpy as np
import random
from collections import namedtuple, deque

import torch

# ==============================================================================
# Contents
# ------------------------------------------------------------------------------

__all__ = [
        # ======================
        # Classes
        # ----------------------
            'BaseMemory',
          # --------------------
          # BaseMemory Children
          # --------------------
            'ReplayMemory',
]

# ==============================================================================
# Variables
# ------------------------------------------------------------------------------
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# ==============================================================================
# Classes
# ------------------------------------------------------------------------------
class BaseMemory:
    """Fixed-size buffer to store experience tuples.
     Args:
         Initialization:
            action_size (int): Dimension of each action.
            memory_size (int): Maximum size of buffer.
            batch_size (int): Size of each training batch.
            window_size (int): Reward window size for averaging short-term progress.
            seed (int): Random seed.
            
        Add (new experience):
            state (array): 
            action (int): 
            reward (int or float): 
            next_state (array): 
            done (boolean): 
    """

    def __init__(self, action_size,
                        memory_size,
                        batch_size=64,
                        window_size=100,
                        seed=0
        ):
        """Initialize a ReplayBuffer object."""
        self.action_size = action_size
        self.memory_size = memory_size
        self.memory = deque(maxlen=memory_size)
        self.batch_size = batch_size
        self.experience = namedtuple("Experience", field_names=["state", "action", "reward", "next_state", "done"])
        self.seed = random.seed(seed)

        # Store reward history and related values here
        self.current_reward = 0
        self.reward_history = []
        self.window_size = window_size
        self.episode_reward_history = []
        self.avg_reward = 0

    def add(self, state, action, reward, next_state, done):
        """Add a new experience to memory."""
        e = self.experience(state, action, reward, next_state, done)
        self.memory.append(e)
        self.current_reward += reward
        self.reward_history.append(reward)

    def renew(self):
        """ Renew memory, such as at the end of an episode """
        self.episode_reward_history.append(self.current_reward)
        self.avg_reward = np.mean(self.episode_reward_history[-self.window_size:])
        self.current_reward = 0
        
    def sample(self):
        """Randomly sample a batch of experiences from memory."""
        experiences = random.sample(self.memory, k=self.batch_size)
        
        # Expand experiences into constituents and then stack them along the batch channel (N) 
        states = torch.from_numpy(np.vstack([e.state for e in experiences if e is not None])).float().to(device)
        actions = torch.from_numpy(np.vstack([e.action for e in experiences if e is not None])).long().to(device)
        rewards = torch.from_numpy(np.vstack([e.reward for e in experiences if e is not None])).float().to(device)
        next_states = torch.from_numpy(np.vstack([e.next_state for e in experiences if e is not None])).float().to(device)
        dones = torch.from_numpy(np.vstack([e.done for e in experiences if e is not None]).astype(np.uint8)).float().to(device)
        
        return (states, actions, rewards, next_states, dones)

    def __len__(self):
        """Returns the current size of internal memory."""
        return len(self.memory)
    
    def clear(self):
        """Clears all memory stores."""
        self.current_reward = 0
        self.reward_history = []
        self.episode_reward_history = []
        self.avg_reward = 0
        self.memory.clear()
        
# ------------------------------------------------------------------------------
# BaseMemory Children
# ------------------------------------------------------------------------------
class ReplayMemory(BaseMemory):
    """ Augmented Base memory buffer for visual DQN.
         - added prior state stacking when sampling for temporal sequence approximation.
    New Args:
        m_stack (int): Number of prior states to concatenate along the channel axis.
            - For creating stacked states when sampling memories (ie. DQN stacks 4).
    Modified Functions:
        self.sample
    """
    
    def sample(self, m_stack=4):
        """Randomly sample a batch of experiences from memory, including prior states."""
        memory_index = random.sample(range(m_stack-1, len(self.memory)), k=self.batch_size)
        
        # Expand experiences into constituents and then stack them along the batch channel (N) 
        states = torch.from_numpy(np.vstack([np.concatenate([self.memory[i-m].state for m in range(m_stack)], axis=1) for i in memory_index if self.memory[i] is not None])).float().to(device)
        actions = torch.from_numpy(np.vstack([self.memory[i].action for i in memory_index if self.memory[i] is not None])).long().to(device)
        rewards = torch.from_numpy(np.vstack([self.memory[i].reward for i in memory_index if self.memory[i] is not None])).float().to(device)
        next_states = torch.from_numpy(np.vstack([np.concatenate([self.memory[i-m].next_state for m in range(m_stack)], axis=1) for i in memory_index if self.memory[i] is not None])).float().to(device)
        dones = torch.from_numpy(np.vstack([self.memory[i].done for i in memory_index if self.memory[i] is not None]).astype(np.uint8)).float().to(device)

        return (states, actions, rewards, next_states, dones)
    
# ------------------------------------------------------------------------------
