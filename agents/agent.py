#!/usr/bin/env python3
""" Agent classes and related functions """
__author__ = 'Henry A Leopold'

# ==============================================================================
# Imports
# ------------------------------------------------------------------------------
import numpy as np
import random

from .model import *
from .memory import ReplayMemory, BaseMemory as ReplayBuffer
from utils import filesystem as fs

import torch
import torch.nn.functional as F
from torch import optim, nn, distributions as dst

# ==============================================================================
# Contents
# ------------------------------------------------------------------------------
#
__all__ = [
        # ===================
        # Classes
        # -------------------
            'BaseAgent',
        # -------------------
        # BaseAgent Children
        # -------------------
            'VisualAgent',
        # ------VA Child-----
            'DQNAgent',
]

# ==============================================================================
# Variables
# ------------------------------------------------------------------------------

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# ==============================================================================
# Classes
# ------------------------------------------------------------------------------
class BaseAgent():
    """ Base agent class based on DQN.
    Args:
        state_size (int): Dimension of each state.
        action_size (int): Dimension of each action.
        memory_size (int): Replay buffer size.
        reward_window (int): Size of rewards to average over for logging progress.
        batch_size (int): minibatch size.
        gamma (float): Discount factor hyperparameter.
        tau (float): Hyperparameter for soft update of target parameters.
        epsilon (float): starting value of epsilon, for epsilon-greedy action selection.
        epsilon_end (float): minimum value of epsilon.
        epsilon_decay (float): multiplicative factor (per episode) for decreasing epsilon.
        learning_rate (float or torch.lr_schedule): Initital learning rate or  / equivalent.
        network_update (int): Number of steps for between updating the network.
        filepath (string): Save/load file path without extension.
        seed (int): Random seed.
    """

    def __init__(self, state_size,
                       action_size,
                       memory_size = 1e5,
                       reward_window = 100,
                       batch_size = 64,
                       gamma = 0.99,
                       tau = 1e-3,
                       epsilon = 1.0,
                       epsilon_end = 0.01,
                       epsilon_decay = 0.995,
                       learning_rate = 5e-4,
                       network_update = 4,
                       filepath = 'agent-checkpoint',
                       seed = 0,
                       **kwargs
                ):
        """Initialize an Agent object."""
        self.state_size = int(state_size)
        self.action_size = int(action_size)
        self.batch_size = int(batch_size)
        self.memory_size = int(memory_size)
        self.reward_window = reward_window
        self.gamma = gamma
        self.tau = tau
        self.epsilon = epsilon
        self.epsilon_start = epsilon
        self.epsilon_end = epsilon_end
        self.epsilon_decay = epsilon_decay
        self.learning_rate = learning_rate
        self.network_update = network_update
        self.filepath = filepath
        self.seed = seed #random.seed(seed)
        self.kwargs = kwargs
        
        # Secondary initialization scripts for augmenting children.
        self.init_memory()
        self.init_learning_rate()
        self.init_qnetwork()
        
    # .................... Wrapper initializers for inheritance ....................
    def init_qnetwork(self):
        """ Network initialization.
        DQN uses an online and 'target' (herein 'internal') networks to alleviate instability in solving an equation with the same equation.
        """
        self.qnetwork_online = QNetwork(self.state_size, self.action_size, self.seed).to(device)
        self.qnetwork_internal = QNetwork(self.state_size, self.action_size, self.seed).to(device)
        self.optimizer = optim.Adam(self.qnetwork_online.parameters(), lr=self.learning_rate)

    def init_memory(self):
        """ Initialize agent's memory buffer.
        DQN uses Replay memory, which randomly samples the agent's entire history. """
        self.memory = ReplayBuffer(self.action_size, self.memory_size, self.batch_size, self.reward_window, self.seed)
    
    def init_learning_rate(self):
        """ Function for augmenting childrens' learning rate initialization. """
        pass
    
    # .................... Python Class Related ........................
    def __getattr__(self, k):
        """If the env has failed to match an attribute, check internal memory."""
        if k in self.__dict__:
            return getattr(self, k)
        else:
            return getattr(self.memory, k)

    # .................... Step and learning policy ....................
    def step(self, state, action, reward, next_state, done):
        """ What the agent does during each step in an episode """
        # Save experience in replay memory
        self.memory.add(state, action, reward, next_state, done)

        # Run the learning policy
        self.policy()

    def policy(self):
        """ Policy function """
        # Initialize time step (for updating every 'network_update' steps)
        if 't_step' not in self.__dict__:
            self.t_step = 0
            print('Initialized time step')

        # Learn every self.network_update time steps.
        self.t_step = (self.t_step + 1) % self.network_update
        if self.t_step == 0:
            # If enough samples are available in memory, get random subset and learn
            if len(self.memory) > self.batch_size:
                experiences = self.memory.sample()
                self.learn(experiences)
                
    # ................. Behaviour, Sensing and Learning ....................
    
    def observe(self, state):
        """ Preprocessing the state inputs before network evaluation.
        Args:
            state (image/array): Current observable environmental state.
        Returns:
            Vectorized / preprocessed state.
        """
        return torch.from_numpy(state).float().unsqueeze(0).to(device)
    
    def act(self, state):
        """Returns actions for given state as per current policy.
        Args:
            state (image/array): Current observable environmental state.
        Returns:
            action (int): Action index for the agent to enact in the current step.
        """
        state = self.observe(state)
        self.qnetwork_online.eval()
        with torch.no_grad():
            action_values = self.qnetwork_online(state)
        self.qnetwork_online.train()

        return self.action_selection(action_values)
        
    def learn(self, experiences):
        """Update value parameters using given batch of experience tuples.
        Args:
            experiences (Tuple[torch.Tensor]): tuple of (s, a, r, s', done) tuples
        """
        states, actions, rewards, next_states, dones = experiences

        # Get max predicted Q values (for next states) from internal model
        Q_next = self.qnetwork_internal(next_states).detach().max(1)[0].unsqueeze(1)

        # Compute Q internal for current states
        Q_internal = rewards + (self.gamma * Q_next * (1 - dones))

        # Get expected Q values from online model
        Q_expected = self.qnetwork_online(states).gather(1, actions)

        # Compute loss
        loss = F.mse_loss(Q_expected, Q_internal)

        # Minimize the loss
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        # Update internal network
        self.soft_update(self.qnetwork_online, self.qnetwork_internal)

    def soft_update(self, online_model, internal_model):
        """Soft update model parameters: θ_internal = τ*θ_online + (1 - τ)*θ_internal
        Args:
            online_model (PyTorch model): Weights will be learned by and copied from
            internal_model (PyTorch model): Internal slow learning the weights are copied to
        """
        for target_param, local_param in zip(internal_model.parameters(), online_model.parameters()):
            target_param.data.copy_(self.tau*local_param.data + (1.0-self.tau)*target_param.data)
    
    # .................... Action Selection / Epsilon ......................
    
    def action_selection(self, action_values):
        """ Action selection policy. Base uses epsilon-greedy action selection """
        if random.random() > self.epsilon:
            return self.exploit(action_values)
        else:
            return self.explore(action_values)

    def explore(self, action_values):
        """ Explore action strategy """
        return random.choice(np.arange(self.action_size))

    def exploit(self, action_values):
        """ Exploit action strategy """
        return np.argmax(action_values.cpu().data.numpy())

    def update_epsilon(self):
        """ Policy for updating epsilon """
        self.epsilon = max(self.epsilon_end, self.epsilon_decay*self.epsilon)

    def end_episode(self):
        """ Protocol for ending an episode """
        self.update_epsilon()
        self.memory.renew()
        
    # .................... Save/Load, Utility & Checkpoint Related .....................
    
    def save(self, filepath=None):
        """ Saves the agent """
        if filepath is None: filepath = self.filepath
        agent_dict = {k:v for k,v in self.__dict__.items() if any(type(v) is x for x in [int,float,list,str])}
        mem_dict = {f'memory.{k}':v for k,v in self.__dict__.items() if any(type(v) is x for x in [int,float,list,str])}
        fs.save_pickle({**agent_dict,**mem_dict}, filepath=filepath+'/agent_config')
        fs.save_pickle(np.stack(self.memory.memory[i] for i in range(len(self.memory.memory))), filepath=self.filepath+'/agent_memory')
        torch.save(self.qnetwork_online.state_dict(), filepath+'/network_weights.pth')
        print(f'Agent saved to {filepath}.')

    def load(self, filepath=None):
        """ Loads the agent """
        if filepath is None: filepath = self.filepath
        self._config = fs.read_file(self._config, filepath=filepath+'agent_config.md')
        #TODO reset all k:v with _config.
        self.memory.memory = fs.load_pickle(filepath+'/agent_memory.pickle')
        self.qnetwork_online.load_state_dict(torch.load(filepath+'/network_weights.pth'))
        print(f'Agent loaded from {filepath}.')

    def reset(self):
        """ Resets the agent's memory and reinitialized the networks """
        raise NotImplementedError

# ------------------------------------------------------------------------------
# Child classes
# ------------------------------------------------------------------------------
class VisualAgent(BaseAgent):
    """ Instead of converting inputs into vectors, this agent uses a CNN as with the original DQN.
    Aside from the network, the hyperparameters are the same as DQN.
    
    New Args:
        m_stack (int): Number of states to concatenate to predict/learn trajectories from
        learning_rate_minimum (float):
        learning_rate_decay (float):
        learning_rate_decay_step (float):
    """
    def __init__(self, m_stack=4, learning_rate_minimum = 0.00025, learning_rate_decay = 0.96, learning_rate_decay_step = 5e5, **kwargs):
        self.m_stack = m_stack
        super(VisualAgent, self).__init__(**kwargs)
        
    def init_memory(self):
        """ Initialize agent's memory buffer.
        DQN uses Replay memory, which randomly samples the agent's entire history. """
        self.memory = ReplayMemory(self.action_size, self.memory_size, self.batch_size, self.reward_window,  self.seed)

    def observe(self, state):
        """ Preprocessing the state inputs before network evaluation.
        Args:
            state (image/array): Current observable environmental state. 
        Returns:
            Preprocessed state.
        """
        state_stack = np.concatenate([state,*(self.memory.memory[-1-i].state for i in range(self.m_stack-1))], axis=1)
        return torch.from_numpy(state_stack).float().to('cuda')

    def policy(self):
        """ Policy function """
        # Initialize time step (for updating every 'network_update' steps)
        if 't_step' not in self.__dict__:
            self.t_step = 0
            print('Initialized time step')
        
        # Learn every self.network_update time steps.
        self.t_step = (self.t_step + 1) % self.network_update
        if self.t_step == 0:
            # If enough samples are available in memory, get random subset and learn
            if len(self.memory.memory) > self.batch_size:
                experiences = self.memory.sample(self.m_stack)
                self.learn(experiences)

    def act(self, state):
        """Returns actions for given state as per current policy.
        Args:
            state (image/array): Current observable environmental state. 
        Returns:
            action (int): Returns the action predicted by the network and/or 
                            selected by the explore-exploit strategy.
        """
        if len(self.memory.memory) < self.m_stack:
            return random_action(range(self.action_size-1))
        state_stack = self.observe(state)
        self.qnetwork_online.eval()
        with torch.no_grad():
            action_values = self.qnetwork_online(state_stack)
        self.qnetwork_online.train()
        
        return self.action_selection(action_values)

    def init_qnetwork(self):
        """ Network initialization. """
        self.feature_extractor = QCNN(input_channels=self.m_stack, seed=self.seed).to(device)
        self.qnetwork_online = nn.Sequential(self.feature_extractor, QNetwork(2048, self.action_size, self.seed, fc1_units=1000, fc2_units=100).to(device))
        self.qnetwork_internal = nn.Sequential(self.feature_extractor, QNetwork(2048, self.action_size, self.seed, fc1_units=1000, fc2_units=100).to(device))
        self.optimizer = optim.Adam(self.qnetwork_online.parameters(), lr=self.learning_rate)

    def action_selection(self, action_values):
        """ Action selection policy. Base uses epsilon-greedy action selection """
        return epsilon_greedy_action(self, action_values)

    def explore(self, action_values):
        """ Explore action strategy """
        return random_action(action_values)

    def exploit(self, action_values):
        """ Exploit action strategy """
        return argmax_action(action_values)

    
class DQNAgent(VisualAgent):
    """ Same as the Visual agent, but uses a different network closer to the DQN paper. """
    def init_qnetwork(self):
        """ Network initialization. """
        self.qnetwork_online = DQN(input_channels=self.m_stack, action_size=self.action_size, seed=self.seed).to(device)
        self.qnetwork_internal = DQN(input_channels=self.m_stack, action_size=self.action_size, seed=self.seed).to(device)
        self.optimizer = optim.Adam(self.qnetwork_online.parameters(), lr=self.learning_rate)

        
        
# ==============================================================================
# Policy functions
# ------------------------------------------------------------------------------
# Sampling methods
# ------------------------------------------------------------------------------
def random_action(actions):
    """Selects action randomly from a distribution.
    Args:
        actions (list): List of available action values in the current state.
    Returns:
        action (int): Action index for the agent to enact in the current step.
    """
    return random.choice(np.arange(len(actions)))

def argmax_action(actions):
    """Selects the action with the largest return.
    Args:
        actions (list): List of available action values in the current state
    Returns:
        action (int): Action index for the agent to enact in the current step.
    """
    return np.argmax(actions.cpu().data.numpy())

def epsilon_greedy_action(agent, actions):
    """Selects epsilon-greedy action for supplied state.
    Args:
        agent (Agent class): Agent passes itself?
        actions (list): List of available action values in the current state.
        epsilon (float): greediness factor.
    Returns:
        action (int): Action index for the agent to enact in the current step.
    """
    if random.random() > agent.epsilon:
        return agent.explore(actions)
    else:
        return agent.exploit(actions)
