#!/usr/bin/env python3
""" Model classes and related functions """
__author__ = 'Henry Leopold'

# ==============================================================================
# Imports
# ------------------------------------------------------------------------------
import torch
import torch.nn.functional as F
from torch import nn, optim

# ==============================================================================
# Contents
# ------------------------------------------------------------------------------

__all__ = [
        # ===================
        # Variables
        # -------------------
            'activations',
        # ===================
        # Classes
        # -------------------
          # Feature Extractors
          # -----------------
            'CNN',
            'QCNN',
          # -----------------
          # Classifiers
          # -----------------
            'QNetwork',
            'DQN',

]

# ==============================================================================
# Variables
# ------------------------------------------------------------------------------
activations = {'tahn', nn.Tanh(),
               'sig', nn.Sigmoid(),
                'relu', nn.ReLU(inplace=True),
                'elu', nn.ELU(inplace=True),
                'lrelu', nn.LeakyReLU(inplace=True),
                'prelu', nn.PReLU()}

# ==============================================================================
# Classes
# ------------------------------------------------------------------------------
# Feature Extractors
# ------------------------------------------------------------------------------
class CNN(nn.Module):
    """ 3 layer convolutional NN with batch normalization for feature extraction. 
    Args:
        Initialization:
            input_channels (int): Channel dimension that may also be a temporal stack of states.
            seed (int): Random seed.
        forward step:
            state (array): Environment state, typically encoded as a numpy array.
    
    Returns:
        Tensor of features extracted from the state.
    """
    def __init__(self, input_channels=1, seed=0):
        super(CNN, self).__init__()
        self.seed = torch.manual_seed(seed)
        self.conv1 = nn.Conv2d(input_channels, 16, kernel_size=5, stride=2)
        self.bn1 = nn.BatchNorm2d(16)
        self.conv2 = nn.Conv2d(16, 32, kernel_size=5, stride=2)
        self.bn2 = nn.BatchNorm2d(32)
        self.conv3 = nn.Conv2d(32, 32, kernel_size=5, stride=2)
        self.bn3 = nn.BatchNorm2d(32)

    def forward(self, state):
        x = F.relu(self.bn1(self.conv1(state)))
        x = F.relu(self.bn2(self.conv2(x)))
        x = F.relu(self.bn3(self.conv3(x)))
        return x.view(x.size(0), -1)
    
# ------------------------------------------------------------------------------    
class QCNN(nn.Module):
    """ Quick CNN since it has only one batch normalization layer.

    Args:
        Initialization:
            input_channels (int): Channel dimension that may also be a temporal stack of states.
            action_size (int): Dimension of each action.
            seed (int): Random seed.
        forward step:
            state (array): Environment state, typically encoded as a numpy array.

    Returns:
        Tensor of features extracted from the state.
    """
    def __init__(self, input_channels=1, action_size=4, seed=0):
        super(QCNN, self).__init__()
        self.seed = torch.manual_seed(seed)
        self.conv1 = nn.Conv2d(input_channels, 32, kernel_size=8, stride=4) # 84x84 in, 21x21 out
        self.bn1 = nn.BatchNorm2d(32)
        self.conv2 = nn.Conv2d(32, 64, kernel_size=4, stride=3) # 21x21 in, 7x7 out
        self.conv3 = nn.Conv2d(64, 128, kernel_size=3, stride=1)

    def forward(self, state):
        x = F.relu(self.bn1(self.conv1(state)))
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))
        return x.view(x.size(0), -1)

                   
# ------------------------------------------------------------------------------
# Classification modules/networks
# ------------------------------------------------------------------------------
class QNetwork(nn.Module):
    """ 3 layer fully connected NN for classification and action selection. 
    Args:
        Initialization:
            state_size (int): Dimension of each state.
            action_size (int): Dimension of each action.
            seed (int): Random seed.
            fc1_units (int): Number of nodes in first hidden layer.
            fc2_units (int): Number of nodes in second hidden layer.
        forward step:
            state (array): Environment state, typically encoded as a numpy array.
    
    Returns:
        Action selection probabilities from the final layer.
    """

    def __init__(self, state_size, action_size, seed, fc1_units=64, fc2_units=64):
        """Initialize parameters and build model."""
        super(QNetwork, self).__init__()
        self.seed = torch.manual_seed(seed)
        self.fc1 = nn.Linear(state_size, fc1_units)
        self.fc2 = nn.Linear(fc1_units, fc2_units)
        self.fc3 = nn.Linear(fc2_units, action_size)

    def forward(self, state):
        """Build a network that maps state -> action values."""
        x = F.relu(self.fc1(state))
        x = F.relu(self.fc2(x))
        return self.fc3(x)
                   
# ------------------------------------------------------------------------------
class DQN(nn.Module):
    """ The QCNN plus one fully connected layer for classification (same as paper).
    
     Args:
        Initialization:
            input_channels (int): Channel dimension that may also be a temporal stack of states.
            action_size (int): Dimension of each action.
            seed (int): Random seed.
        forward step:
            state (array): Environment state, typically encoded as a numpy array.
    
    Returns:
        Action selection probabilities from the fully connected layer.
    """
    def __init__(self, input_channels=1, action_size=4, seed=0):
        super(DQN, self).__init__()
        self.seed = torch.manual_seed(seed)
        self.conv1 = nn.Conv2d(input_channels, 32, kernel_size=8, stride=4) # 84x84 in, 21x21 out
        self.bn1 = nn.BatchNorm2d(32)
        self.conv2 = nn.Conv2d(32, 64, kernel_size=4, stride=3) # 21x21 in, 7x7 out
        self.conv3 = nn.Conv2d(64, 64, kernel_size=3, stride=1)
        self.fc = nn.Linear(1024, action_size)
        
    def forward(self, state):
        x = F.relu(self.bn1(self.conv1(state)))
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))
        return self.fc(x.view(x.size(0), -1))
                   
# ------------------------------------------------------------------------------