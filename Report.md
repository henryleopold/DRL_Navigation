# Project report
The algorithm solved the environemtn in 381 episodes (0-index for 100 episode long window). The corresponding plot:

![Average episodic rewards during training](assets/baseAgent_project.png)

Also, a plot of training over 2000 episodes:

![Training over 2000 episodes](assets/baseAgent_batch64_qNet64x64-2000episodes.png)

## Learning Algorithm
The algorithm for the main project is the based on the DQN exercise/solution. It uses a series of 3 fully-connected layers (the 
'*QNetwork*' to classify the vector state space input. An online training network was copied to a seperate internal network every 4 episodes, inline with DQN. Adam was used to optimize learning. For the Visual learners, a CNN was prepended to the the online and internal *QNetworks* to extract features from the state inputs, which included the 3 preceeding state images in the channel dimension (inline with DQN).

## Hyperparameters
Hyperparamters have been adjusted to be inline with the [DQN paper](https://storage.googleapis.com/deepmind-media/dqn/DQNNaturePaper.pdf). The hyperparameters presented in that work, have been partially incorporated into the Visual/pixel environment, wherein the *Y* channel was used to sample the state. Other preprocessing included shifting the intensity distribution from (0,1) to (-.5,.5), though the Visual environment challenge is still under development. 

## Code Design / Future Work
The agent classes and training scripts have been designed for both visual and vector based environments, and work smoothly for either. They have been tested for *OpenAI Gym* and *Unity ML-Agents*. The agent and memory classes are designed for inheritance with the intention of expanding the algorithms for DDQN, Rainbow, etc., though this is for the future. Notably, the memory class has been augmented to contain scores and reward history and the agent manages its own epsilon, allowing for inheritance and modification of learning strategies as well as making the training scripts much more legible. Future exploration of memory strategies - particularly *hindsight experience replay* - are of great interest. 

Developing a headless docker was more time-consuming than expected, but proved useful for the both vector and visual/pixel environments. The images are [available](https://hub.docker.com/r/henryleopold/drlnd/), however the dockerfiles are still in progress. The docker images will be valuable going forward with the nanodegree.