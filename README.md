# DRLND_project1
Training a DRL Agent to navigate a modified Unity3D Banana environment. This is the first project for the [Deep Reinforcement Learning Nanodegree](https://www.udacity.com/course/deep-reinforcement-learning-nanodegree--nd893) using [PyTorch](http://docs.pytorch.org/), [Unity ML-Agents](https://github.com/Unity-Technologies/ml-agents/blob/master/docs/Installation.md) and [NumPy](http://www.numpy.org/).

# Project Details
**Project Environment**: Unity3D ML-Agents Banana Environment (non-default, Udacity).

**Project Variant**: Vector based environment (Navigate.ipynb)

**Challenge Variant**: Visual based environment (Navigate_Pixels.ipynb)

Both variants have an action space of 4 and a single state space. The environment is considered solved when the agent received an average reward of 13.0 or higher over the last 100 episodes; the first episode in the window is used as the index episode for success.

# Getting Started
## Quick start
A docker image was created for running DRLND experiments, which can be found [here](https://hub.docker.com/r/henryleopold/drlnd/). It requires NVIDIA-Cuda and [nvidia-docker](https://github.com/NVIDIA/nvidia-docker) and the correct Banana environment be downloaded and extracted into the *assets* folder.

To start, first modify the local project directory to match your project folder (mine is '~/Projects/drlnd') and then run:
```
docker run --runtime=nvidia -it -p 8888:8888 -v ~/Projects/drlnd:/root/drlnd  henryleopold/drlnd:unity
```

## Package Installation 
Please refer to the instructions for setting up the [DRLND source](https://www.udacity.com/course/deep-reinforcement-learning-nanodegree--nd893) and [Unity ML-Agents](https://github.com/Unity-Technologies/ml-agents/blob/master/docs/Installation.md) repositories.

Instructions for installing nvidia-docker are [here](https://github.com/NVIDIA/nvidia-docker).

# Instructions
Once the docker is running, connect to the jupyter notebook and train using the Navigate_\*.ipynb of interest. The build, agent and training config variables, which are python dictionaries defininng parameters. Tweak the dictionaries within the notebooks to modify hyperparameters.
